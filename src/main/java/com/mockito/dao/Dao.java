package com.mockito.dao;

import java.util.List;

import com.mockito.dto.Usuario;

public interface Dao {

	public List<Usuario> dameUsuario();
	
}
