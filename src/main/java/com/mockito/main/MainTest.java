package com.mockito.main;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.mockito.dao.Dao;
import com.mockito.dto.Usuario;
import com.mockito.servicio.ServicioImpl;


// Usaremos la consola Test de Mockito
@RunWith(MockitoJUnitRunner.class)
public class MainTest{
	
	// Cargaremos esta lista con datos simulados
	List<Usuario> listaUsuarios = null;

	@InjectMocks
	private ServicioImpl servicio;

	@Mock
	private Dao dao;
	
	// Preparamos los datos antes de ejecutar el test
	@Before
	public void prepare() {
		
		listaUsuarios = new ArrayList<Usuario>();
		listaUsuarios.add(new Usuario("Pepe", "Garc�a", "39"));
		listaUsuarios.add(new Usuario("Nelson", "Mandela", "37"));
		listaUsuarios.add(new Usuario("Anbis", "Yataka", "2"));
		listaUsuarios.add(new Usuario("Cleo", "Patris", "32"));

		when(dao.dameUsuario()).thenReturn(listaUsuarios);
	}
	
	@Test
	public void testPrueba() {
		
		// Cogemos un registro para hacer pruebas
		Usuario usuario = servicio.dameUsuario().get(0);
	
		// Comprobamos que un dato sea el que esperamos
		Assert.assertEquals(usuario.getNombre(), "Pepe");
		
		// Comprueba que no hayan nulos v�a Lambda!!
		listaUsuarios.forEach((obj) -> Assert.assertNotNull(obj.getNombre()));
		
		// Verdadero o falso
		Assert.assertTrue(usuario.getNombre().length() < 10);
		
	}	
	
				

}
