package com.mockito.servicio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mockito.dao.Dao;
import com.mockito.dto.Usuario;

@Service
public class ServicioImpl implements Servicio{

	@Autowired
	private Dao dao;
	
	public List<Usuario> dameUsuario() {
		return dao.dameUsuario();
	}
}
